<?php 
include 'koneksi.php';
$db = new database();

$aksi = $_GET['aksi'];
if($aksi == "m_insert"){
    $filename = "DC_".date("YmdHis").".".pathinfo(basename($_FILES["file"]["name"]),PATHINFO_EXTENSION);
    $tempname = $_FILES['file']['tmp_name'];
    $hasil=$db->insertdata($_POST['id_masker'],$_POST['nama'],$_POST['warna'],$_POST['tipe_wajah'],$tempname,$filename);
    if($hasil['respon']=="sukses"){
        header("location:masker.php?pesan=insertsuccess");
    }else{
        header("location:masker.php?pesan=insertfailed");
    }
}elseif($aksi == "m_update"){
    $filename = $_POST['photos'];
    $tempname = $_FILES['file']['tmp_name'];
    $hasil=$db->updatedata($_POST['id_masker'],$_POST['nama'],$_POST['warna'],$_POST['tipe_wajah'],$tempname,$filename);
    if($hasil['respon']=="sukses"){
        header("location:masker.php?pesan=updatesuccess");
    }else{
        header("location:masker.php?pesan=updatefailed");
    }
}elseif($aksi == "m_delete"){ 	
    $hasil=$db->deletedata($_GET['id_masker']);
    if($hasil['respon']=="sukses"){
        header("location:masker.php?pesan=deletesuccess");
    }else{
        header("location:masker.php?pesan=deletefailed");
    }
}
?>