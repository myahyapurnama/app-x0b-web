<?php
    $DB_NAME = "tokomasker";
    $DB_USER = "root";
    $DB_PASS =  "";
    $DB_SERVER_LOC = "localhost";

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
        $mode = $_POST['mode'];
        $respon = array(); $respon['kode'] = '000';
        switch($mode){
            case "insert":
                $id_masker = $_POST["id_masker"];
                $nama = mysqli_real_escape_string($conn,trim($_POST["nama"]));
                $warna = $_POST["warna"];
                $tipe_wajah = $_POST["tipe_wajah"];
                $imstr = $_POST["image"];
                $file = $_POST["file"];
                $path = "images/";

                $sql = "insert into masker(id_masker, nama, warna, tipe_wajah, photos) values(
                    '$id_masker','$nama','$warna','$tipe_wajah','$file')";
                $result = mysqli_query($conn,$sql);
                if($result){
                    if(file_put_contents($path.$file,base64_decode($imstr)) == false){
                        $sql = "delete from masker where id_masker='$id_masker'";
                        mysqli_query($conn,$sql);
                        $respon['kode'] = "111";
                        echo json_encode($respon); exit();    
                    }else{
                        echo json_encode($respon); exit(); //insert data sukses semua
                    }
                }else{
                    $respon['kode'] = "111";
                    echo json_encode($respon); exit();
                }
                
            break;
            case "update":
                $id_masker = $_POST["id_masker"];
                $nama = mysqli_real_escape_string($conn,trim($_POST["nama"]));
                $warna = $_POST["warna"];
                $tipe_wajah = $_POST["tipe_wajah"];
                $imstr = $_POST["image"];
                $file = $_POST["file"];
                $path = "images/";

                if($imstr==""){
                    $sql = "UPDATE masker SET nama='$nama',warna='$warna',tipe_wajah='$tipe_wajah'
                    where id_masker='$id_masker'";
                    $result = mysqli_query($conn,$sql);
                    if($result){
                        echo json_encode($respon); exit();
                    }else{
                        $respon['kode'] = "111";
                        echo json_encode($respon); exit();
                    }
                }else{
                    if(file_put_contents($path.$file,base64_decode($imstr)) == false){
                        $respon['kode'] = "111";
                        echo json_encode($respon); exit();    
                    }else{
                        $sql = "UPDATE masker SET nama='$nama',warna='$warna',tipe_wajah='$tipe_wajah',photos='$file'
                                where id_masker='$id_masker'";
                        $result = mysqli_query($conn,$sql);
                        if($result){
                            echo json_encode($respon); exit(); //update data sukses semua
                        }else{
                            $respon['kode'] = "111";
                            echo json_encode($respon); exit();
                        }
                    }
                }
            break;
            case "delete":
                $id_masker = $_POST["id_masker"];
                $sql = "SELECT photos from masker where id_masker='$id_masker'";
                $result = mysqli_query($conn,$sql);
                if($result){
                    if(mysqli_num_rows($result)>0){
                        $data = mysqli_fetch_assoc($result);
                        $photos = $data['photos'];
                        $path = "images/";
                        unlink($path.$photos);
                    }
                    $sql = "DELETE from masker where id_masker='$id_masker'";
                    $result = mysqli_query($conn,$sql);
                    if($result){
                        echo json_encode($respon); exit(); //delete data sukses
                    }else{
                        $respon['kode'] = "111";
                        echo json_encode($respon); exit();
                    }
                }
            break;
        }
    }
?>