<?php
    $DB_NAME = "tokomasker";
    $DB_USER = "root";
    $DB_PASS =  "";
    $DB_SERVER_LOC = "localhost";

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
        $nama = mysqli_real_escape_string($conn,trim($_POST["nama"]));
        $sql = "SELECT m.id_masker,m.nama,m.warna,m.tipe_wajah,concat('http://192.168.1.8/tokomasker/images/',m.photos) as url
		FROM masker m
        WHERE m.nama like '%$nama%'";
        $result = mysqli_query($conn,$sql);
        if(mysqli_num_rows($result)>0){
            header("Access-Control-Allow-Origin: *");
            header("Content-type: application/json; charset=UTF-8");

            $data_member = array();
            while($member = mysqli_fetch_assoc($result)){
                array_push($data_member,$member);
            }
            echo json_encode($data_member);
        }
    }
?>