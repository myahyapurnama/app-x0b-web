<?php
    include 'koneksi.php';
    $db = new database();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Toko Masker - Update Data</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
    <!-- Navbar -->
	<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <div class="container">
        <a class="navbar-brand" href="index.php">Toko Masker</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link" href="index.php">Home</a>
                <a class="nav-item nav-link" href="about.php">About</a>
                <a class="nav-item nav-link active" href="member.php">Masker <span class="sr-only">(current)</span></a>
            </div>
        </div>
        </div>
    </nav>
    <!-- Navbar End -->
<div class="container">
<h4 class="mt-3 mb-3 text-primary">Update Data Masker</h4>
<?php foreach($db->editdata($_GET['id_masker']) as $mem) : ?>
<form action="proses.php?aksi=m_update" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-8 col-md-offset-1">
        <div class="form-group">
            <label for="id_masker">ID</label>
            <input type="hidden" id="id_masker" name="id_masker" value="<?= $mem['id_masker'] ?>"><br>
            <?= $mem['id_masker'] ?>
        </div>
        <div class="form-group">
            <label for="nama">Nama Masker</label>
            <input type="text" placeholder="Masukkan Nama" id="nama" name="nama" class="form-control" required value="<?= $mem['nama'] ?>">
        </div>
        <div class="form-group">
            <label for="warna">Warna</label>
            <input type="text" placeholder="Masukkan Warna" id="warna" name="warna" class="form-control" required value="<?= $mem['warna'] ?>">
        </div>
        <div class="form-group">
            <label for="tipe_wajah">Level</label>
            <select class="form-control" name="tipe_wajah">
                <option value="Kering" <?= ($mem['tipe_wajah']=="Kering")? "selected" : "" ?>>Kering</option>
                <option value="Normal" <?= ($mem['tipe_wajah']=="Normal")? "selected" : "" ?>>Normal</option>
                <option value="Lembab"    <?= ($mem['tipe_wajah']=="Lembab")? "selected" : "" ?>>Lembab</option>
            </select>
        </div>
        <div class="form-group">
            <label for="photos">Foto</label><br>
            <img src="<?= $mem['url'] ?>" width="100px" height="100px" />
            <input type="hidden" id="photos" name="photos" value="<?= $mem['photos'] ?>">
            <input type="file" class="form-control-file" id="file" name="file">
        </div>

        <button type="submit" class="btn btn-primary">Simpan</button>
        <a href="masker.php" class="btn btn-primary">Batal</a>
        </div>
</form>
<?php endforeach ?>
</div>
</div>
<script src="js/jquery-3.4.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>