<?php
    include 'koneksi.php';
    $db = new database();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Toko Masker - Tambah Data</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
    <!-- Navbar -->
	<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <div class="container">
        <a class="navbar-brand" href="index.php">Toko Masker</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link" href="index.php">Home</a>
                <a class="nav-item nav-link" href="about.php">About</a>
                <a class="nav-item nav-link active" href="masker.php">Masker <span class="sr-only">(current)</span></a>
            </div>
        </div>
        </div>
    </nav>
    <!-- Navbar End -->
<div class="container">
<h4 class="mt-3 mb-3 text-primary">Tambah Data Masker</h4>
<form action="proses.php?aksi=m_insert" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-8 col-md-offset-1">
        <div class="form-group">
            <label for="nim">ID</label>
            <input type="text" placeholder="Masukkan ID Masker" id="id_masker" name="id_masker" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="nama">Nama Masker</label>
            <input type="text" placeholder="Masukkan Nama" id="nama" name="nama" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="telepon">Warna</label>
            <input type="text" placeholder="Masukkan Warna" id="warna" name="warna" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="level">Tipe Wajah</label>
            <select class="form-control" name="level">            
                <option value="Kering">Kering</option>          
                <option value="Normal">Normal</option>
                <option value="Lembab">Lembab</option>
            </select>
        </div>
        <div class="form-group">
            <label for="foto">Foto</label>
            <input type="file" class="form-control-file" id="file" name="file">
        </div>
        <button type="submit" class="btn btn-primary">Simpan</button>
        <a href="masker.php" class="btn btn-primary">Batal</a>
        </div>
</form>
</div>
</div>
<script src="js/jquery-3.4.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>