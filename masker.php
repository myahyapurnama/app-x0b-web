<?php
    include 'koneksi.php';
    $db = new database();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Toko Masker - Masker</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
    <!-- Navbar -->
	<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <div class="container">
        <a class="navbar-brand" href="index.php">Toko Masker</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link" href="index.php">Home</a>
                <a class="nav-item nav-link" href="about.php">About</a>
                <a class="nav-item nav-link active" href="masker.php">Masker <span class="sr-only">(current)</span></a>
            </div>
        </div>
        </div>
    </nav>
    <!-- Navbar End -->
    <div class="container">
    <h3 class="mt-3 mb-4 text-center text-primary">DAFTAR MASKER</h3>
    <?php
    if(isset($_GET['pesan'])){
        if($_GET['pesan'] == "insertsuccess"){
            $msg = "sukses ditambahkan";
            $tipe = "success";
        }else if($_GET['pesan'] == "updatesuccess"){
            $msg = "sukses diedit";
            $tipe = "success";
        }else if($_GET['pesan'] == "deletesuccess"){
            $msg = "sukses dihapus";
            $tipe = "success";
        }else if($_GET['pesan'] == "insertfailed"){
            $msg = "gagal ditambahkan";
            $tipe = "danger";
        }else if($_GET['pesan'] == "updatefailed"){
            $msg = "gagal diedit";
            $tipe = "danger";
        }else if($_GET['pesan'] == "deletefailed"){
            $msg = "gagal dihapus";
            $tipe = "danger";
        }
        echo '<div class="alert alert-'.$tipe.' alert-dismissible fade show" role="alert">
                Data Masker <strong>'.$msg.'</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
    }
    ?>
    <table class="table">
    <thead>
        <tr class="bg-primary text-light text-center">
        <th scope="col">ID</th>
        <th scope="col">Nama Masker</th>
        <th scope="col">Warna</th>
        <th scope="col">Tipe Wajah</th>
        <th scope="col">Foto</th>
        <th scope="col">Aksi</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($db->tampildata() as $mem) : ?>
        <tr>
            <td class="text-center"><?= $mem['id_masker'] ?></td>
            <td><?= $mem['nama'] ?></td>
            <td class="text-center"><?= $mem['warna'] ?></td>
            <td class="text-center"><?= $mem['tipe_wajah'] ?></td>
            <td class="text-center"><img src="<?= $mem['url'] ?>" width="100px" height="100px" /></td>
            <td class="text-center">
                <a href="update.php?id_masker=<?php echo $mem['id_masker']; ?>" class="btn btn-primary">Edit</a>
                <a href="proses.php?id_masker=<?php echo $mem['id_masker']; ?>&aksi=m_delete" class="btn btn-primary">Hapus</a>
			</td>
        </tr>
    <?php endforeach ?>
    </tbody>
    </table>
    <a href="insert.php" class="btn btn-primary mb-3">
    Tambah Masker
    </a>
</div>
<script src="js/jquery-3.4.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>