<?php
    class database{
        public $host = "localhost",
        $uname = "root",
        $pass = "",
        $db = "tokomasker",
        $con,
        $path = "images/";
        

        public function __construct()
        {   
            $this->con = mysqli_connect($this->host,$this->uname,$this->pass,$this->db);
            date_default_timezone_set('Asia/Jakarta');
        }

        public function tampildata(){
            $sql = "SELECT m.id_masker,m.nama,m.warna,m.tipe_wajah, concat('http://localhost/tokomasker/images/',photos) as url
                FROM masker m";
            $data = mysqli_query($this->con,$sql);
            while ($d = mysqli_fetch_array($data))
            {
                $hasil[] = $d;
            }
            return $hasil;
        }


        public function insertdata($id_masker,$nm,$warna,$tipe_wajah,$imstr,$file){
            $nama=mysqli_real_escape_string($this->con,trim($nm));
            $sql = "INSERT into masker(id_masker, nama, warna, tipe_wajah, photos) values(
                '$id_masker','$nama','$warna', '$tipe_wajah','$file')";
            $result=mysqli_query($this->con,$sql);
            if($result){
                if(move_uploaded_file($imstr,$this->path.$file) == false){
                    $sql = "delete from masker where id_masker='$id_masker'";
                    mysqli_query($this->con,$sql);
                    $hasil['respon']="gagal";
                    return $hasil;   
                }else{
                    $hasil['respon']="sukses";
                    return $hasil;
                }
            }else{
                $hasil['respon']="gagal";
                return $hasil;
            }
        }

        public function editdata($id_masker){         // fungsi mengambil data
            $sql = "SELECT m.id_masker,m.nama,m.warna,m.tipe_wajah,m.photos,concat('http://localhost/tokomasker/images/',photos) as url
                FROM masker m
                WHERE m.id_masker ='$id_masker'"; 
            $data = mysqli_query($this->con,$sql);
            while ($d = mysqli_fetch_array($data))
            {
                $hasil[] = $d;
            }
            return $hasil;
        }

        public function updatedata($id_masker,$nm,$warna,$tipe_wajah,$imstr,$file){ // fungsi mengudapte data
            $nama=mysqli_real_escape_string($this->con,trim($nm));
            if($imstr==""){
                $sql = "UPDATE masker SET nama='$nama', warna='$warna', tipe_wajah='$tipe_wajah'
                where id_masker='$id_masker'";
                $result = mysqli_query($this->con,$sql);
                if($result){
                    $hasil['respon']="sukses";
                    return $hasil; //update data sukses tanpa foto
                }else{
                    $hasil['respon']="gagal";
                    return $hasil;
                }
            }else{
                if(move_uploaded_file($imstr,$this->path.$file) == false){
                    $hasil['respon']="gagal";
                    return $hasil;  
                }else{
                    $sql = "UPDATE masker SET nama='$nama',warna='$warna',tipe_wajah='$tipe_wajah',photos='$file'
                            where id_masker='$id_masker'";
                    $result = mysqli_query($this->con,$sql);
                    if($result){
                        $hasil['respon']="sukses";
                        return $hasil; //update data sukses semua
                    }else{
                        $hasil['respon']="gagal";
                        return $hasil;
                    }
                }
            }
        }      
        public function deletedata($id_masker){
            $sql = "SELECT photos from masker where id_masker='$id_masker'";
            $result = mysqli_query($this->con,$sql);
            if($result){
                if(mysqli_num_rows($result)>0){
                    $data = mysqli_fetch_assoc($result);
                    $photos = $data['photos'];
                    unlink($this->path.$photos);
                }
                $sql = "DELETE from masker where id_masker='$id_masker'";
                $result = mysqli_query($this->con,$sql);
                if($result){
                    $hasil['respon']="sukses";
                    return $hasil; //delete data sukses
                }else{
                    $hasil['respon']="gagal";
                    return $hasil;
                }
            }
        }
    }
?>